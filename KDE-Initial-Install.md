# FEDORA KDE INSTALL

The installation guide includes instructions on how to boot and install the necessary drivers for systems that use Nvidia graphics. Nvidia graphics are not supported by default since the drivers are proprietary. If you do not use Nvidia graphics, then you may skip the [LIVE USB](#live-usb) section.

## LIVE USB

- Does not have Nvidia Drivers
- Requires Nouveau Drivers before boot
    - On Grub Menu press "e"
    - Add following command after the line ending with 'rhgb quiet'
        - ``modprobe.blacklist="nouveau"``
    - Press F10 and Install Fedora

## Initial Install
- Update System
- Install RPM Fusion Free & Non-Free respositories
    - ``sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm``
    - ``sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm``
- Update DNF
    - ``sudo dnf makecache``
- Install Nvidia Drivers
    - ``sudo dnf install akmod-nvidia``
- Reboot system

## Install Google Chrome Repository
- Enable Third Party Respositories
    - ``sudo dnf install fedora-workstation-reposittories``
- Enable google-chrome repository
    - ``sudo dnf config-manager --set-enabled google-chrome``
- Install Chrome
    - ``sudo dnf install google-chrome-stable``


## Install Applications

NOTE: Make sure to enable third party repositories before installing steam.

``sudo dnf install htop vim git gimp inkscape go python3-devel.x86_64 cowsay neofetch godot java-11-openjdk.x86_64 deja-dup zsh steam keepassxc kdenlive``


## Install Development Tools Group
``sudo dnf groupinstall "Development Tools" "Development Libraries" ``


## Add & Install [Codium](https://vscodium.com/)
```
sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg 
printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscodium.repo 
sudo dnf install codium
```


## Add the Flathub repository
``flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo``


## Install Flatpaks
``flatpak install flathub com.discordapp.Discord com.github.jeromerobert.pdfarranger com.github.tchx84.Flatseal com.mojang.Minecraft com.obsproject.Studio com.spotify.Client org.telegram.desktop org.ferdium.Ferdium io.dbeaver.DBeaverCommunity com.github.Murmele.Gittyup fr.handbrake.ghb``


## Backup/Restore from NAS
- KDE does not mount Samba shares to system. Manually mount network drive
    - Custom bash script created to mount/unmount the "Backups" share folder

## Virtual Machines
- Enable virtualization for CPU within BIOS
- Install Virtualization Software: [View Fedora Docs](https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-virtualization/) 
    - ``sudo dnf group install --with-optional virtualization``

