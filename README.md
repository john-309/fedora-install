# Fedora Install

This repo contatins instructions on how to install and/or rebuild Fedora the way it is configured on my workstation. You will find different sets of installation instructions, depending on the desktop environment you choose.


### Choose Desktop Environment

* [KDE Variant](KDE-Initial-Install.md)
* [GNOME Variant](Gnome-Initial-Install.md)

