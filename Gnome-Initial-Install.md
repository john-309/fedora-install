# Fedora Gnome Install (Default Desktop Environment)

## Install Applications
NOTE: Make sure to enable third party repositories before installing steam.

`sudo dnf install htop vim git gimp inkscape go python3-devel.x86_64 gnome-tweaks menulibre sl cowsay fortune-mod.x86_64 neofetch tilix virtualbox-guest-additions godot java-11-openjdk.x86_64 deja-dup zsh curl wget unzip ufw dkms steam keepassxc`


## Install Development Tools Group
`sudo dnf groupinstall "Development Tools" "Development Libraries"`


## Add the Flathub repository
`flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo`


## Add & Install [Codium](https://vscodium.com/)
```
sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg 
printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscodium.repo 
sudo dnf install codium 
```


## Github Downloads
`Terminal Countdown (https://github.com/antonmedv/countdown/)`


## Additional Extensions
* [AppIndicator](https://extensions.gnome.org/extension/615/appindicator-support/) 
* Screen Autorotate


